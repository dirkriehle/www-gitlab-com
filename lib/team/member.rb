module Gitlab
  module Homepage
    class Team
      class Member
        def initialize(data)
          @data = data
        end

        def username
          @data.fetch('gitlab')
        end

        def country
          @data.fetch('country')
        end

        def story
          @data.fetch('story')
        end

        def involved?(project)
          roles.has_key?(project.key)
        end

        def roles
          @roles ||= Hash.new.tap do |hash|
            @data.fetch('projects', {}).each do |project, roles|
              Array(roles).each { |role| (hash[project] ||= []) << role }
            end
          end
        end
      end
    end
  end
end
